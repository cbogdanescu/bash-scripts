# Copyright (C) 2014 Institute e-Austria Timisoara (Romania)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#!/bin/sh

#get Ubuntu Cloud image location
if [[ $EUID -ne 0 ]]; then
   $(zenity --error --title='ERROR' --text="To execute this script root privileges are necessary. Please login as root");
   exit 1;
fi
image=$(zenity --file-selection --file-filter="*.iso" --title="Please select a Ubuntu image");
ks=$(zenity --file-selection --file-filter="ks.cfg" --title="Please select your kickstart file (ks.cfg)");
if [ ! -z $image ]; then
#create tempLinuxISO folder
$(mkdir /tmp/tempLinuxISO);
#mount Ubuntu image inside tempLinuxISO
$(mount -o loop $image /tmp/tempLinuxISO);
#create finalLinuxISO folder
$(mkdir /tmp/finalLinuxISO);
#copy Ubuntu image files
$(rsync -aH /tmp/tempLinuxISO/ /tmp/finalLinuxISO/) | $(zenity --progress --title="Creating ISO image"  --no-cancel --auto-close --pulsate --width=330 --text="Please wait while the ISO is being created"); 
#copy kickstart file
$(cp $ks /tmp/finalLinuxISO/);
#add Kickstart installation label
$(printf "label install-using-kickstart \n  menu label ^Install Ubuntu using Kickstart \n  kernel /install/vmlinuz \n  append  file=/cdrom/preseed/ubuntu.seed initrd=/install/initrd.gz ks=cdrom:/ks.cfg --" >> /tmp/finalLinuxISO/isolinux/txt.cfg);
#custom image ISO name
customImage="UbuntuKickstartImage.iso";
cd /tmp/finalLinuxISO;
$(mkisofs -D -r -V "UbuntuKickstartImage" -cache-inodes -J -l -b "isolinux/isolinux.bin" -c "isolinux/boot.cat" -no-emul-boot -boot-load-size 4 -boot-info-table -o $customImage .);
$(umount /tmp/tempLinuxISO);
fi
