KSUbuntu
========

About
-----
KSUbuntu is a bash script which creates a Ubuntu image containing a user-defined kickstart script used for an unattended install.

Dependencies
------------
 **Debian-based Linux (eg: Ubuntu 12.04)**
 
 **zenity package (for GUI dialog boxes)**

How to run
----------
::

    bash ksubuntu.sh
