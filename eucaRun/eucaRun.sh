# Copyright (C) 2014 Institute e-Austria Timisoara (Romania)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#!/bin/bash

options=$(getopt -o e:,u:,g:,hV -l "emi:,userdata:,securitygroup:,help,version" -- "$@");
eval set -- "$options";
eucalyptusPath=${!#};
while true; do
  case "$1" in
  -e|--emi)
      shift;
      if [ -n "$1" ]; then
		emi=$1;
	shift;
      fi
      ;;
  -u|--userdata)
      shift;
      if [ -n "$1" ]; then
                userData=$1;
        shift;
      fi
      ;;
  -g|--securitygroup)
      shift;
      if [ -n "$1" ]; then
                securityGroup=$1;
        shift;
      fi
      ;;
  -h|--help)
      echo "";
      echo "Usage:";
      echo "./eucaRun.sh [OPTIONS] eucalyptus_configuration_folder_path";
      echo "Starts a Eucalyptus instance using the provided userdata";
      echo "";
      echo "Options:";
      echo "-e, --emi            The EMI (Eucalyptus Machine Image) used for starting the instance";
      echo "-g, --securitygroup  The security group used for starting the instance";
      echo "-h, --help           Output help information and exit";
      echo "-u, --userdata       The user-data file used for starting the instance";
      echo "-V, --version        Output version information and exit";
      echo "";
      exit 0;
	;;
  -V|--version)
      echo "eucaRun 1.0.0";
      echo "Written by Caius Bogdanescu";
      exit 0;
	;;
  *)
      if [ $eucalyptusPath = "--" ]; then
        echo "[ERROR] Please provide a path to the Eucalyptus configuration folder. Use -h for details";
        exit 1;
      else break;
      fi
      ;;
  esac
done
#Initialize the Eucalyptus environment
echo "[INFO] Initializing Eucalyptus environment";
source $eucalyptusPath"/eucarc";
eucalyptusResult=$?;
if [ $eucalyptusResult -eq 1 ]; then
echo "[ERROR] An invalid Eucalyptus path was provided!";
exit 0;
#check options
elif [ $eucalyptusResult -eq 0 ]; then
  if [ -z $emi ]; then
    #print error
    echo "[ERROR] The [--emi] and [--userdata] options are mandatory. Use -h for details";
    exit 1;
  elif [ -z $userData ]; then
    #print error
    echo "[ERROR] The [--emi] and [--userdata] options are mandatory. Use -h for details";
    exit 1;
  elif [ -z $securityGroup ]; then
    #set default security group
    echo "[WARNING] You didn't provide a security group. Do you want to use 'default' security group? [Y/N]";
    read answer;
    if [ answer == "Y" || answer == "y"  ]; then
      securityGroup="default";
      authorize=$(euca-authorize default -P tcp -p 22 -s 0.0.0.0/0);
    elif  [ answer == "N" || answer == "n"  ]; then
    echo "[ERROR] Setting a security group is mandatory. Use -h for help";
    exit 1;
    else
    echo "[ERROR] Your answer was invalid. Use Y/N when providing your answer";
    exit 1;

  fi
fi
echo "[INFO] Starting instance using $emi";
#create eucaRunKP keypair
if [ ! -f $eucalyptusPath"/eucaRunKP" ]; then
  keyPair=$(euca-add-keypair eucaRunKP > $eucalyptusPath"/eucaRunKP" && chmod 600 $eucalyptusPath"/eucaRunKP");
fi
#start new instance
instance=$(euca-run-instances -n 1 -g $securityGroup -k eucaRunKP -t m1.medium -f $userData --addressing private $emi);
sleep 3;
#get instance ID
instanceID=$(echo $instance | grep -o "\bi-[A-Z0-9]\+");
echo  "[INFO] Instance ID: $instanceID";
#get instance status
status=$(euca-get-console-output $instanceID | tail -2 | head -1 | tr -d "\n" | tr -d "\r");
printf "[INFO] Please be patient while the instance boots up";
#check instance status every 10s
while true
do
	if [ "$status" = "FINISHED" ]; then
	  echo "";
	  break;
	else
	  printf ".....";
	  status=$(euca-get-console-output $instanceID | tail -2 | head -1 | tr -d "\n" | tr -d "\r");
	  sleep 10;
	fi
done
fi
tempIP=$(euca-allocate-address);
#allocate IP address to Eucalyptus
ip=$(echo $tempIP | cut -d " " -f2);
#associate IP address to instance
address=$(euca-associate-address -i $instanceID $ip);
sleep 4;
keygen=$(ssh-keygen -f "$HOME/.ssh/known_hosts" -R $ip 2> /dev/null);
#print results
echo "[RESULT]";
echo "[INFO] For details about the instance execute the following:";
echo "-> source $eucalyptusPath/eucarc";
echo "-> euca-describe-instances $instanceID";
echo "[INFO] To login into $instanceID execute the following:";
echo "-> ssh -i $eucalyptusPath/eucaRunKP ubuntu@$ip";
echo "[INFO] To delete $instanceID execute the following:";
echo "-> source $eucalyptusPath/eucarc";
echo "-> euca-terminate-instances $instanceID";
echo "[RESULT]";
