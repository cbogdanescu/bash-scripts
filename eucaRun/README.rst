eucaRun
=======

About
-----
eucaRun is a bash script created to start an instance on a Eucalyptus using the provided (minimalistic) user-data file.

Dependencies
------------
 **Debian-based Linux (eg: Ubuntu 12.04)**
 
 **euca2ools package**

 **userData file (minimalistic version)**

 **Eucalyptus credentials**

How to run
----------
::

    bash eucaRun.sh -e EMI -u USER_DATA_LOCATION [-g SECURITY_GROUP] EUCALYPTUS_CREDENTIALS_LOCATION