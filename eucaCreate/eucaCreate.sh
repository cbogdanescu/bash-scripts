# Copyright (C) 2014 Institute e-Austria Timisoara (Romania)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#!/bin/sh

#get Ubuntu Cloud image location
if [[ $EUID -ne 0 ]]; then
   $(zenity --error --title='ERROR' --text="To execute this script root privileges are necessary. Please login as root");
   exit 1;
fi
image=$(zenity --file-selection --file-filter="*.img" --title="Please select a Ubuntu Cloud image");
if [ ! -z $image ]; then
  #get Eucalyptus image size
  imageSize=$(zenity --scale --text "Please select the desired Eucalyptus image size (in GB)" --min-value=5 --max-value=10 --value=5 --step 1);
  #create Eucalyptus image folder
  $(mkdir eucaImage);
  #create Eucalyptus image
  $(dd if=/dev/zero of="eucaImage/eucaImage.img" bs=1G count=$imageSize) | $(zenity --progress --title="Creating Eucalyptus image"  --no-cancel --auto-close --pulsate --width=330 --text="Please wait while the image is being created...");
  #get Ubuntu Cloud image loop device
  loopSourceDevice=$(losetup -f);
  #assosicate Ubuntu Cloud image to loop device
  $(losetup $loopSourceDevice $image);
  #get Eucalyptus image loop device
  loopDestinationDevice=$(losetup -f);
  #associate Eucalyptus image to loop device
  $(losetup $loopDestinationDevice  "eucaImage/eucaImage.img");
  #create Eucalyptus image EXT4 filesystem
  mkfs=$(mkfs.ext4 -L euca-image $loopDestinationDevice);
  #get image UUID
  imageUUID=$(blkid -o value -s UUID $loopDestinationDevice);
  #create Ubuntu Cloud image mount point
  $(mkdir /tmp/eucaSource);
  #create Eucalyptus image mount point
  $(mkdir /tmp/eucaDestination);
  #mount Ubuntu Cloud image
  $(mount $loopSourceDevice /tmp/eucaSource);
  #mount empty Eucalyptus image
  $(mount $loopDestinationDevice /tmp/eucaDestination);
  #copy Ubuntu Cloud image files to Eucalyptus image
  $(rsync -aH /tmp/eucaSource/ /tmp/eucaDestination/) | $(zenity --progress --title="Populating Eucalyptus image"  --no-cancel --auto-close --pulsate --width=330 --text="Please wait while the image is being populated...");
  #add fstab with euca-image label
  $(echo "UUID=$imageUUID / ext4 defaults 1 1" > /tmp/eucaDestination/etc/fstab);
  #copy Eucalyptus image ERI
  $(rsync -aH /tmp/eucaDestination/boot/initrd* eucaImage/) | $(zenity --progress --title="Copying Eucalyptus ERI"  --no-cancel --auto-close --pulsate --width=330 --text="Please wait while the ERI is being copied...");
  #copy Eucalyptus image EKI
  $(rsync -aH /tmp/eucaDestination/boot/vmlinuz* eucaImage/) | $(zenity --progress --title="Copying Eucalyptus EKI"  --no-cancel --auto-close --pulsate --width=330 --text="Please wait while the EKI is being copied...");
  #unmount Ubuntu Cloud image
  $(umount /tmp/eucaSource);
  #unmount Eucalyptus image
  $(umount /tmp/eucaDestination);
  #detach Ubuntu Cloud image
  $(losetup -d $loopSourceDevice);
  #detach Eucalyptus image
  $(losetup -d $loopDestinationDevice);
  #remove Ubuntu Cloud image mount point
  $(rmdir /tmp/eucaSource);
  #remove Eucalyptus image mount point
  $(rmdir /tmp/eucaDestination);
  user=$(zenity --entry --text="Please enter a valid username for changing the image ownership to that user");
  if [ ! -z $user ]; then
    #search for user in passwd
    search=$(cut -d':' -f1 "/etc/passwd" | grep $user);
    if [ $? -eq 0 ]; then
      #change eucaImage folder ownership
      $(chown -R $user "eucaImage/");
    fi
  fi
  #print success message to user
  $(zenity --info --text="Your Eucalyptus image has been succesfully created.");
fi
