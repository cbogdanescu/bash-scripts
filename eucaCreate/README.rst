eucaCreate
==========

About
-----
eucaCreate is a bash script which creates a Eucalyptus image from an existing Ubuntu Cloud image.

Dependencies
------------
 **Debian-based Linux (eg: Ubuntu 12.04)**
 
 **zenity package (for GUI dialog boxes)**

How to run
----------
::

    bash eucaCreate.sh